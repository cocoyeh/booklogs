from django.http import Http404
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from logs.models import Log
from logs.models import Reader
from logs.serializers import LogSerializer
from logs.serializers import ReaderSerializer


class ReaderList(APIView):

    def post(self, request, format=None):
        serializer = ReaderSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ReaderDetail(APIView):

    def get_object(self, pk):
        try:
            return Reader.objects.get(pk=pk)
        except Reader.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        reader = self.get_object(pk)
        serializer = ReaderSerializer(reader)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        reader = self.get_object(pk)
        serializer = ReaderSerializer(reader, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def patch(self, request, pk, format=None):
        reader = self.get_object(pk)
        serializer = ReaderSerializer(reader, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        reader = self.get_object(pk)
        reader.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class LogList(APIView):

    def get_object(self, readerId):
        try:
            return Log.objects.filter(readerId=readerId)
        except Log.DoesNotExist:
            return None

    def get(self, request, pk, format=None):
        logs = self.get_object(readerId=pk)
        if logs is not None:
            serializer = LogSerializer(logs, many=True)
            return Response(serializer.data)
        else:
            return Response("{}", status=status.HTTP_400_BAD_REQUEST)

    def post(self, request, pk, format=None):
        logs = self.get_object(readerId=pk)
        serializer = LogSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LogDetail(APIView):

    def get_object(self, logId):
        try:
            return Log.objects.get(pk=logId)
        except Log.DoesNotExist:
            raise Http404

    def get(self, request, pk, pk2, format=None):
        log = self.get_object(pk2)
        serializer = LogSerializer(log)
        return Response(serializer.data)

    def put(self, request, pk, pk2, format=None):
        log = self.get_object(pk2)
        serializer = LogSerializer(log, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def patch(self, request, pk, pk2, format=None):
        log = self.get_object(pk2)
        serializer = LogSerializer(log, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, pk2, format=None):
        log = self.get_object(pk2)
        if log is not None:
            log.delete()
        return Response(status=status.HTTP_400_BAD_REQUEST)
