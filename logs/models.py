from django.db import models
from pygments.lexers import get_all_lexers
from pygments.styles import get_all_styles

LEXERS = [item for item in get_all_lexers() if item[1]]
LANGUAGE_CHOICES = sorted([(item[1][0], item[0]) for item in LEXERS])
STYLE_CHOICES = sorted((item, item) for item in get_all_styles())


class Reader(models.Model):
    readerId = models.AutoField(primary_key=True)
    created = models.DateTimeField(auto_now_add=True)
    appleId = models.CharField(max_length=128, blank=True)
    email = models.CharField(max_length=128, blank=True)
    password = models.CharField(max_length=128, blank=True)
    uuid = models.CharField(max_length=36, blank=True)
    name = models.CharField(max_length=128, blank=False)
    photoPath = models.CharField(max_length=128, blank=True, default='')
    goal = models.PositiveSmallIntegerField(default=0)


class Log(models.Model):
    logId = models.AutoField(primary_key=True)
    # TODO: make ForeignKey works
    # readerId = models.ForeignKey(Reader, on_delete=models.CASCADE, default='')
    readerId = models.PositiveSmallIntegerField()
    created = models.DateTimeField(auto_now_add=True)
    title = models.CharField(max_length=128, blank=True, default='')
    author = models.CharField(max_length=128, blank=True, default='')
    note = models.CharField(max_length=1024, blank=True)
    ISBN = models.CharField(max_length=13, blank=True, default='')
