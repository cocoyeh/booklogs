from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns

from logs import views

urlpatterns = [
    url(r'^reader/$', views.ReaderList.as_view()),
    url(r'^reader/(?P<pk>[0-9]+)/$', views.ReaderDetail.as_view()),
    url(r'^reader/(?P<pk>[0-9]+)/log/$', views.LogList.as_view()),
    url(r'^reader/(?P<pk>[0-9]+)/log/(?P<pk2>[0-9]+)/$', views.LogDetail.as_view()),
    url(r'^rest-auth/', include('rest_auth.urls')),
    url(r'^rest-auth/registration/', include('rest_auth.registration.urls')),
]

urlpatterns = format_suffix_patterns(urlpatterns)
