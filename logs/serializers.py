from rest_framework import serializers
from rest_framework.validators import UniqueTogetherValidator

from logs.models import Log
from logs.models import Reader


class ReaderSerializer(serializers.ModelSerializer):

    def validate_goal(self, value):

        # Assuming readerId is present in the request and serializer
        try:
            readerId = self.initial_data['readerId']
            reader = Reader.objects.get(readerId=readerId)
            original_goal = reader.goal
        except KeyError:
            raise serializers.ValidationError("Please set readerId in the payload")

        if value < original_goal:  # check name has more than 1 word
            raise serializers.ValidationError(
                "Please set a goal greater or equal then the previous one")
        return value

    class Meta:
        # How to bubble up UniqueTogetherValidator error
        model = Reader
        fields = ('readerId', 'created', 'appleId', 'email', 'password', 'uuid', 'name', 'photoPath', 'goal')
        validators = [
            UniqueTogetherValidator(
                queryset=Reader.objects.all(),
                fields=('appleId', 'name')
            )
        ]


class LogSerializer(serializers.ModelSerializer):
    class Meta:
        model = Log
        fields = ('logId', 'readerId', 'created', 'title', 'author', 'note', 'ISBN')
        validators = [
            UniqueTogetherValidator(
                queryset=Log.objects.all(),
                fields=('readerId', 'title', 'author')
            )
    ]
